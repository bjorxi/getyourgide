from tornado.web import RequestHandler


class SearchHandler(RequestHandler):
    def initialize(self, db):
        self.db = db

    def get(self, query_string):
        print("Query String:", query_string)
        r = self.db.search(query_string)
        self.write(dict(
            size = len(r),
            entries = r
        ))
