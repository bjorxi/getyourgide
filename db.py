class DB(object):
    def __init__(self, entries=None):
        self.entries = [] if entries is None else entries

    def search(self, query_string):
        """
        >>> DB([(1, "", "tour city",), (2, "", "some other",)]).search("city tour")
        [(1, '', 'tour city')]
        """
        terms = query_string.split()
        print("Terms:", terms)
        result = []

        for e in self.entries:
            target = len(terms)
            for t in terms:
                if t in e[2]:
                    target -= 1
            if target == 0:
                result.append(e)

        return result

    def add(self, entry):
        """Addes an entry to the DB

        Args:
            entry:tuple: a tuple of (id, datatime, text)
        """
        self.entries.append(entry)
