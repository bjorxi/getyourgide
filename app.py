#!/usr/bin/env python3

import json
import logging

from tornado.web import Application
from tornado.ioloop import IOLoop

from db import DB
from handlers import SearchHandler


logging.basicConfig(format="%(asctime)s (%(name)s) [%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def initdb(filename="data.json"):
    db = DB()

    with open(filename, "r") as f:
        for l in f:
            q_dict = json.loads(l)
            for e in q_dict["data"]:
                db.add((e["review_id"], e["date"], e["message"]))

    return db


def main():
    logger.info("Loading data")
    db = initdb()
    logger.info("Creating application")
    app = Application([
        (r"/s/(.+)", SearchHandler, dict(db=db)),
    ])
    app.listen(8888)
    logger.info("Listening on 8888")
    IOLoop.current().start()


if __name__ == "__main__":
    main()
